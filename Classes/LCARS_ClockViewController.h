//
//  LCARS_ClockViewController.h
//  LCARS Clock
//
//  Created by Danny Draper on 12/07/2010.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//	this is another test comment

#import <UIKit/UIKit.h>
#import "ClockView.h"

@interface LCARS_ClockViewController : UIViewController {
	IBOutlet ClockView *clockview;
}

@property (retain, nonatomic) ClockView *clockview;

@end

