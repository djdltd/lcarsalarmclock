//
//  ClockView.h
//  LCARS Clock
//
//  Created by Danny Draper on 15/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClockNumbers.h"
#import "AlarmNumbers.h"
#import "SecondNumbers.h"
#import "DaysTemplate.h"
#import "MonthsTemplate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>

typedef enum {
	HourUp,
	HourDown,
	MinuteUp,
	MinuteDown,
	None
} AlarmAdjustMode;

typedef enum {
	ClockButton,
	ClockButtonHigh,
	AlarmSound1,
	AlarmSound2,
	AlarmSound3,
	AlarmSound4,
	AlarmSound5,
	AlarmSound6,
	AlarmSound7,
	AlarmSound8,
	AlarmSound9,
	AlarmSound10
} ClockSound;

@interface ClockView : UIView<AVAudioPlayerDelegate>  {
	
	// We need these outlets to the buttons on the nib so that when
	// the iphone is rotated to portrait are landscape then we
	// can reposition the buttons in the correct locations
	IBOutlet UIButton *portraitbtn_announcetime;
	IBOutlet UIButton *landscapebtn_announcetime;
	IBOutlet UIButton *btn_hourup;
	IBOutlet UIButton *btn_hourdown;
	IBOutlet UIButton *btn_minuteup;
	IBOutlet UIButton *btn_minutedown;
	IBOutlet UIButton *btn_snd;
	IBOutlet UIButton *btn_alarmonoff;
	IBOutlet UIButton *btn_dismiss;
	IBOutlet UIButton *btn_snooze;
	
	IBOutlet UIImageView *img_alert;
	IBOutlet UIImageView *img_alarmtime;
	IBOutlet UIImageView *img_alarmactive;
	IBOutlet UIImageView *img_alarmoff;
	//IBOutlet UIImageView *img_snd1;
	//IBOutlet UIImageView *img_snd2;
	IBOutlet UIImageView *img_am;
	IBOutlet UIImageView *img_pm;
	
	UIImage *alarmsoundlabel;
	
	bool _landscapemode;
	bool _alarmactive;
	//bool _usingsound1;
	bool _alertactive;
	bool _audioplaying;
	
	int _currentalarmminute;
	int _currentalarmhour;
	NSInteger _numericmonth;
	NSInteger _numericweekday;
	
	int _currentminute;
	int _currenthour;
	bool _12hourmode;
	bool _ispm;
	bool _alarmadjustedbytimer;
	bool _userdismissedalert;
	CGPoint _alarmsoundlabel_location;

	NSMutableString *_strtime;
	
	ClockNumbers *_clocknumbers;
	NSTimer *_clockticktimer;
	NSTimer *_alarmadjusttimer;
	NSTimer *_alertflashtimer;
	NSTimer *_snoozetimer;
	NSTimer *announceTimer;
	
	SystemSoundID _singlesound;
	
	AlarmNumbers *_alarmnumbers;
	SecondNumbers *_secondnumbers;
	SecondNumbers *_daynumbers;
	SecondNumbers *_yearnumbers;
	DaysTemplate *_daystemplate;
	MonthsTemplate *_monthstemplate;
	
	NSDateFormatter* _formatter;
	NSCalendar *_gregorian;
	NSLocale *_uslocale;
	AlarmAdjustMode _alarmadjustmode;
	bool _timeannouncementplaying;
	int _iannouncenumber;
	int _currentalarmsound;
	int _currentbrightness;
	
	AVAudioPlayer* _myaudioplayer;
}


@property (retain, nonatomic) UIButton *portraitbtn_announcetime;
@property (retain, nonatomic) UIButton *landscapebtn_announcetime;
@property (retain, nonatomic) UIButton *btn_hourup;
@property (retain, nonatomic) UIButton *btn_hourdown;
@property (retain, nonatomic) UIButton *btn_minuteup;
@property (retain, nonatomic) UIButton *btn_minutedown;
@property (retain, nonatomic) UIButton *btn_snd;
@property (retain, nonatomic) UIButton *btn_alarmonoff;
@property (retain, nonatomic) UIButton *btn_dismiss;
@property (retain, nonatomic) UIButton *btn_snooze;

@property (retain, nonatomic) UIImageView *img_alert;
@property (retain, nonatomic) UIImageView *img_alarmtime;
@property (retain, nonatomic) UIImageView *img_alarmactive;
@property (retain, nonatomic) UIImageView *img_alarmoff;
//@property (retain, nonatomic) UIImageView *img_snd1;
//@property (retain, nonatomic) UIImageView *img_snd2;
@property (retain, nonatomic) UIImageView *img_am;
@property (retain, nonatomic) UIImageView *img_pm;


- (void)loadSettings;
- (void)saveSettings;
- (void)playNumericsound:(int)numeric;
- (void)play12Hournumeric:(int)numeric;
- (void)triggerAnnounce:(id)sender;
- (IBAction)btnHourmode:(id)sender;
- (IBAction)toggleAlarmOnOff:(id)sender;
- (void)HourAdjust:(bool)down;
- (void)MinuteAdjust:(bool)down;
- (IBAction)btnDismiss:(id)sender;
- (IBAction)btnSnooze:(id)sender;
- (IBAction)alarmAdjustcancel:(id)sender;
- (IBAction)btnMinuteuppressed:(id)sender;
- (IBAction)btnMinutedownpressed:(id)sender;
- (IBAction)btnHourdownpressed:(id)sender;
- (IBAction)btnHouruppressed:(id)sender;
- (IBAction)btnMinuteup:(id)sender;
- (IBAction)btnMinutedown:(id)sender;
- (IBAction)btnHourdown:(id)sender;
- (IBAction)btnHourup:(id)sender;
- (IBAction)toggleSound:(id)sender;
- (IBAction)btnAnnouncetime:(id)sender;
- (IBAction)alarmSoundBackbuttontriggered:(id)sender;
- (IBAction)alarmSoundNextbuttontriggered:(id)sender;
- (IBAction)btnAdjustbrightness:(id)sender;

- (void)clearAllLocalnotifications;
- (void)scheduleGenericalarm;
- (void)scheduleAlarmForDate:(NSDate*)theDate;
- (void)startAnnouncementTimer;
- (void)playSound:(ClockSound)soundtoplay;
- (void)playSinglesound:(NSString *)resourcename;
- (void)refreshAmpm;
- (void)startSnooze;
- (void)snoozeTick:(id)sender;
- (void)setAlertactive:(bool)active;
- (void)stopAlertflash;
- (void)alertFlashtick:(id)sender;
- (void)startAlertflash;
- (void)setAlarmvisible:(bool)visible;
- (void)alarmAdjust:(AlarmAdjustMode)adjustmode;
- (void)startAlarmadjust;
- (void)RefreshAlarmtime;
- (void)refreshClockTick;
- (void)GetNumericTime;
- (void)startClocktick;
- (void)alarmAdjusttick:(id)sender;
- (void)clockTick:(id)sender;
- (void)setAlarmactive:(bool)active;
- (void)setLandscapemode:(bool)landscape;
- (void)loadAlarmsoundlabel:(int)soundnumber;
- (void)playCurrentalarmsound;
- (void)validateAlarmsound;
- (void) configureAudioServices;
- (void)playSinglesoundEx:(NSString *)resourcename;

@end
