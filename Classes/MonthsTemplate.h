//
//  MonthsTemplate.h
//  LCARS Clock
//
//  Created by Danny Draper on 22/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MonthsTemplate : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	int _xloc;
	int _yloc;
	bool _visible;
	UIImage *frame;
	int _imonth;
	CGFloat _scalefactor;
}

- (void) setVisible:(bool)visible;
- (void) Update;
- (void) Initialise;
- (void) Paint;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;
- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc;
- (void) setNumericMonth:(int)month;

	
@end
