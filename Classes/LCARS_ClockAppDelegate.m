//
//  LCARS_ClockAppDelegate.m
//  LCARS Clock
//
//  Created by Danny Draper on 12/07/2010.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//  This is a test comment

#import "LCARS_ClockAppDelegate.h"
#import "LCARS_ClockViewController.h"

@implementation LCARS_ClockAppDelegate

@synthesize window;
@synthesize viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
	
	return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	AudioSessionSetActive (false);
	[viewController.clockview saveSettings];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	NSLog (@"Application has entered the background!");
	[viewController.clockview scheduleGenericalarm];
	[viewController.clockview saveSettings];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	NSLog (@"Application became active!");
	[viewController.clockview clearAllLocalnotifications];
}

- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
