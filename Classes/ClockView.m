//
//  ClockView.m
//  LCARS Clock
//
//  Created by Danny Draper on 15/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

/*
 
 Version 1.3
 
 - Fixed a nasty AM/PM bug that would manifest if the iPhone 24 hour mode was switched off, same bug the iPad version had.
 
 - Added 8 more sounds. The LCARS Clock now has 10 alarm sounds.
 
 - Changed the Icon title to LCARS Clock instead of LCARS...lock
 
 - The LCARS Clock now manages it's own volume instead of relying on the ringer volume.
 
 - The Alarm will now work when the iPhone is switched to silent.
 
 - Added a Brightness Adjustment feature. Tap the Display Brightness button to toggle between 5 brightness levels.
 
 
 Version 1.3.1
 
 - Added a prefs synchronize when saving settings
 
 - Fixed a bug whereby the clock would display the incorrect year on 1st Jan.
 
 */

#import "ClockView.h"


@implementation ClockView
@synthesize portraitbtn_announcetime;
@synthesize landscapebtn_announcetime;

@synthesize btn_hourup; // 26 x 27, 392, 104
@synthesize btn_hourdown;  // 26 x 27, 364, 104
@synthesize btn_minuteup;  // 26 x 27, 446, 104
@synthesize btn_minutedown;  // 26 x 27, 418, 104
@synthesize btn_snd; // 40 x 32, 336, 180
@synthesize btn_alarmonoff; // 40 x 32, 336, 216
@synthesize img_alarmtime; // 70 x 52, 288, 51
@synthesize img_alarmactive;
@synthesize img_alarmoff;
//@synthesize img_snd1;
//@synthesize img_snd2;
@synthesize img_am;
@synthesize img_pm;
@synthesize btn_dismiss;
@synthesize btn_snooze;
@synthesize img_alert;


- (id)initWithCoder:(NSCoder *)coder
{
	if ((self = [super initWithCoder:coder])) {
		
		// Default the alarm to inactive
		_alarmactive = false;
		_alertactive = false;
		[self setAlarmactive:_alarmactive];
		
		[self setAlarmvisible:false];
		
		[_clocknumbers = [ClockNumbers new] retain];
		[_clocknumbers Initialise];
		
		
		[_alarmnumbers = [AlarmNumbers new] retain];
		[_alarmnumbers Initialise];
		[_alarmnumbers setVisible:true];
		
		[_secondnumbers = [SecondNumbers new] retain];
		//[_secondnumbers Initialise];
		[_secondnumbers Initialise:1.0f :1.0f :12.0f :29.0f :12.0f :@"SecondNumbers"];
		
		[_secondnumbers setVisible:true];
		
		[_daynumbers = [SecondNumbers new] retain];
		[_daynumbers Initialise:2.0f :1.0f :9.0f :23.0f :9.0f :@"SmallNumbers"];
		[_daynumbers setVisible:true];
		
		[_yearnumbers = [SecondNumbers new] retain];
		[_yearnumbers Initialise:2.0f :1.0f :9.0f :23.0f :9.0f :@"SmallNumbers"];
		[_yearnumbers setVisible:true];
		
		[_daystemplate = [DaysTemplate new] retain];
		[_daystemplate Initialise];
		[_daystemplate setVisible:true];
		
		[_monthstemplate = [MonthsTemplate new] retain];
		[_monthstemplate Initialise];
		[_monthstemplate setVisible:true];
		
		[_formatter = [NSDateFormatter new] retain];
		_gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
		
		
		[_uslocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] retain];
		[_formatter setLocale:_uslocale];
		
		
		[_strtime = [NSMutableString new] retain];
		
		_currentalarmhour = 7;
		_currentalarmminute = 30;
		_userdismissedalert = false;
		_currentalarmsound = 1;
		_currentbrightness = 5;
		
		//alarmsoundlabel.alpha = 0.5f;
		
		[self loadSettings];
		[self validateAlarmsound];
		
		[self loadAlarmsoundlabel:_currentalarmsound];
		
		[self RefreshAlarmtime];
		
		[self startClocktick];
		
		[self setLandscapemode:_landscapemode];
		
		[self configureAudioServices];
		
		//self.alpha = 0.1f;
	}
	
	return self;
}

- (void)saveSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	if (prefs != nil) {
	
		[prefs setBool:_alarmactive forKey:@"lcarsclock_alarmactive"];
		[prefs setBool:_12hourmode forKey:@"lcarsclock_12hourmode"];
		//[prefs setBool:_usingsound1 forKey:@"lcarsclock_alarmsound1"];
		[prefs setInteger:_currentalarmsound forKey:@"lcarsclock_currentalarmsound"];
		[prefs setInteger:_currentalarmhour forKey:@"lcarsclock_currentalarmhour"];
		[prefs setInteger:_currentalarmminute forKey:@"lcarsclock_currentalarmminute"];
		[prefs setBool:true forKey:@"lcarsclock_settingspresent"];
		
		[prefs synchronize];
		
		NSLog (@"Settings saved ok.");
	} else {
		NSLog (@"Unable to save settings! Prefs was nil!");
	}
}

- (IBAction)btnAdjustbrightness:(id)sender
{
	[self playSinglesound:@"clockbuttonhigh"];
	
	if (_currentbrightness < 5) {
		_currentbrightness++;
	} else {
		_currentbrightness = 1;
	}
	
	if (_currentbrightness == 1) {
		self.alpha = 0.2f;
	}
	
	if (_currentbrightness == 2) {
		self.alpha = 0.4f;
	}
	
	if (_currentbrightness == 3) {
		self.alpha = 0.6f;
	}
	
	if (_currentbrightness == 4) {
		self.alpha = 0.8f;
	}
	
	if (_currentbrightness == 5) {
		self.alpha = 1.0f;
	}
}

- (void) configureAudioServices {
	

	AudioSessionInitialize (NULL, NULL,	NULL,NULL);
	
	//UInt32 sessionCategory = kAudioSessionCategory_AmbientSound;
	UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
	AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
	
	
	UInt32 audioOverride =  true;
	AudioSessionSetProperty (kAudioSessionProperty_OverrideCategoryMixWithOthers, sizeof (audioOverride), &audioOverride);
	
	AudioSessionSetActive (true);
}

- (void)loadSettings
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	if (prefs != nil) {
	
		bool settingspresent = [prefs boolForKey:@"lcarsclock_settingspresent"];
	
		if (settingspresent == true) {
		
			_alarmactive = [prefs boolForKey:@"lcarsclock_alarmactive"];
			_12hourmode = [prefs boolForKey:@"lcarsclock_12hourmode"];
			//_usingsound1 = [prefs boolForKey:@"lcarsclock_alarmsound1"];
			_currentalarmsound = [prefs integerForKey:@"lcarsclock_currentalarmsound"];
			_currentalarmhour = [prefs integerForKey:@"lcarsclock_currentalarmhour"];
			_currentalarmminute = [prefs integerForKey:@"lcarsclock_currentalarmminute"];
		}
		
		
		
		NSLog (@"Current alarm sound is: %i", _currentalarmsound);
		NSLog (@"Settings loaded ok.");
	} else {
		NSLog (@"Could not load settings! Prefs was nil!");
	}
}

- (void)validateAlarmsound
{
	bool soundnumbervalid = false;
	
	if (_currentalarmsound == 1) {soundnumbervalid = true;}
	if (_currentalarmsound == 2) {soundnumbervalid = true;}
	if (_currentalarmsound == 3) {soundnumbervalid = true;}
	if (_currentalarmsound == 4) {soundnumbervalid = true;}
	if (_currentalarmsound == 5) {soundnumbervalid = true;}
	if (_currentalarmsound == 6) {soundnumbervalid = true;}
	if (_currentalarmsound == 7) {soundnumbervalid = true;}
	if (_currentalarmsound == 8) {soundnumbervalid = true;}
	if (_currentalarmsound == 9) {soundnumbervalid = true;}
	if (_currentalarmsound == 10) {soundnumbervalid = true;}	

	if (soundnumbervalid == false) {
		_currentalarmsound = 1;
	}
}

- (void)scheduleGenericalarm
{
	if (_alarmactive == false) {
		return;
	}
	
	NSDate *now = [NSDate date];
	
	NSCalendar *gregorian = [[NSCalendar alloc]
							 initWithCalendarIdentifier:NSGregorianCalendar];
	
	unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
	
	
	NSDateComponents *alarm_components = [gregorian components:unitFlags fromDate:now];
	
	//int alarmday = [alarm_components day];
	//int alarmmonth = [alarm_components month];
	//int alarmyear = [alarm_components year];
	//int alarmhour = _currentalarmhour;
	//int alarmminute = _currentalarmminute;
	
	[alarm_components setHour:_currentalarmhour];
	[alarm_components setMinute:_currentalarmminute];
	
	//NSLog (@"Alarm Day: %i", alarmday);
	//NSLog (@"Alarm Month: %i", alarmmonth);
	//NSLog (@"Alarm Year: %i", alarmyear);
	//NSLog (@"Alarm Hour: %i", alarmhour);
	//NSLog (@"Alarm Minute: %i", alarmminute);
	
	
	NSDate *alarm_today = [gregorian dateFromComponents:alarm_components];
	
	//NSLog (@"Alarm Date is: %@", alarm_today);
	
	NSDateComponents *addcomp = [[NSDateComponents alloc] init];
	addcomp.day = 1;
	
	NSDate *alarm_tomorrow = [gregorian dateByAddingComponents:addcomp toDate:alarm_today options:0];
	
	//NSLog (@"Alarm Tomorrow: %@", alarm_tomorrow);
	
	if ([alarm_today timeIntervalSinceDate:now] >= 0) {
		//NSLog (@"Alarm today is in the future");
		
		[self scheduleAlarmForDate:alarm_today];
	} else {
		//NSLog (@"Alarm today is in the past");
		
		[self scheduleAlarmForDate:alarm_tomorrow];
	}
	
	//NSDate *now = [NSDate date];
	//NSDate *future = [now dateByAddingTimeInterval:10];
	
	
	
	[gregorian release];
	[addcomp release];
	//[self scheduleAlarmForDate:future];
}

- (void)clearAllLocalnotifications
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
}

- (void)scheduleAlarmForDate:(NSDate*)theDate
{
	UIApplication *app = [UIApplication sharedApplication];
	NSArray *oldNotifications = [app scheduledLocalNotifications];
	
	// Clear out the old notifications before scheduling a new one
	if ([oldNotifications count] > 0) {
		[app cancelAllLocalNotifications];
	}
	
	// Create a new notification
	UILocalNotification* alarm = [[[UILocalNotification alloc] init] autorelease];
	
	if (alarm)
	{
		alarm.fireDate = theDate;
		alarm.timeZone = [NSTimeZone defaultTimeZone];
		alarm.repeatInterval = NSDayCalendarUnit;
		
		//if (_usingsound1 == true) {
			//alarm.soundName = @"clocksound1repeat.wav";
		//} else {
			alarm.soundName = @"clocksound3-repeat.WAV";
		//}
		
		alarm.alertBody = @"Alarm";
		
		[app scheduleLocalNotification:alarm];
	}
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		
    }
	
    return self;
}

- (void)alertFlashtick:(id)sender
{
	if (img_alert.hidden == true) {
		
		[self playCurrentalarmsound];
		
		img_alert.hidden = false;
	} else {
		img_alert.hidden = true;
	} 
}

- (void)startAlertflash
{
	_alertactive = true;
	_alertflashtimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.8 target:self selector:@selector(alertFlashtick:) userInfo:nil repeats:TRUE];
}

- (void)stopAlertflash
{
	if (_alertactive == true) 
	{
		_alertactive = false;
		[_alertflashtimer invalidate];
	}
}

- (void)setAlertactive:(bool)active
{
	if (active == true) {
		[self setAlarmvisible:active];
		[self startAlertflash];
	} else {
		[self stopAlertflash];
		[self setAlarmvisible:active];
	}
}

- (IBAction)btnDismiss:(id)sender
{
	[self playSound:ClockButton];
	_userdismissedalert = true;
	[self setAlertactive:false];
}

- (IBAction)btnSnooze:(id)sender
{
	//_snoozemode = true;
	[self playSound:ClockButton];
	_userdismissedalert = true;
	[self setAlertactive:false];
	[self startSnooze];	
}

- (void)play12Hournumeric:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"12"];}
	if (numeric == 1) {[self playSinglesound:@"1"];}
	if (numeric == 2) {[self playSinglesound:@"2"];}
	if (numeric == 3) {[self playSinglesound:@"3"];}
	if (numeric == 4) {[self playSinglesound:@"4"];}
	if (numeric == 5) {[self playSinglesound:@"5"];}
	if (numeric == 6) {[self playSinglesound:@"6"];}
	if (numeric == 7) {[self playSinglesound:@"7"];}
	if (numeric == 8) {[self playSinglesound:@"8"];}
	if (numeric == 9) {[self playSinglesound:@"9"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"1"];}
	if (numeric == 14) {[self playSinglesound:@"2"];}
	if (numeric == 15) {[self playSinglesound:@"3"];}
	if (numeric == 16) {[self playSinglesound:@"4"];}
	if (numeric == 17) {[self playSinglesound:@"5"];}
	if (numeric == 18) {[self playSinglesound:@"6"];}
	if (numeric == 19) {[self playSinglesound:@"7"];}
	if (numeric == 20) {[self playSinglesound:@"8"];}
	
	if (numeric == 21) {[self playSinglesound:@"9"];}
	if (numeric == 22) {[self playSinglesound:@"10"];}
	if (numeric == 23) {[self playSinglesound:@"11"];}
}

- (void)playNumericsound:(int)numeric
{
	if (numeric == 0) {[self playSinglesound:@"0"];}
	if (numeric == 1) {[self playSinglesound:@"01"];}
	if (numeric == 2) {[self playSinglesound:@"02"];}
	if (numeric == 3) {[self playSinglesound:@"03"];}
	if (numeric == 4) {[self playSinglesound:@"04"];}
	if (numeric == 5) {[self playSinglesound:@"05"];}
	if (numeric == 6) {[self playSinglesound:@"06"];}
	if (numeric == 7) {[self playSinglesound:@"07"];}
	if (numeric == 8) {[self playSinglesound:@"08"];}
	if (numeric == 9) {[self playSinglesound:@"09"];}
	if (numeric == 10) {[self playSinglesound:@"10"];}
	
	if (numeric == 11) {[self playSinglesound:@"11"];}
	if (numeric == 12) {[self playSinglesound:@"12"];}
	if (numeric == 13) {[self playSinglesound:@"13"];}
	if (numeric == 14) {[self playSinglesound:@"14"];}
	if (numeric == 15) {[self playSinglesound:@"15"];}
	if (numeric == 16) {[self playSinglesound:@"16"];}
	if (numeric == 17) {[self playSinglesound:@"17"];}
	if (numeric == 18) {[self playSinglesound:@"18"];}
	if (numeric == 19) {[self playSinglesound:@"19"];}
	if (numeric == 20) {[self playSinglesound:@"20"];}
	
	if (numeric == 21) {[self playSinglesound:@"21"];}
	if (numeric == 22) {[self playSinglesound:@"22"];}
	if (numeric == 23) {[self playSinglesound:@"23"];}
	if (numeric == 24) {[self playSinglesound:@"24"];}
	if (numeric == 25) {[self playSinglesound:@"25"];}
	if (numeric == 26) {[self playSinglesound:@"26"];}
	if (numeric == 27) {[self playSinglesound:@"27"];}
	if (numeric == 28) {[self playSinglesound:@"28"];}
	if (numeric == 29) {[self playSinglesound:@"29"];}
	if (numeric == 30) {[self playSinglesound:@"30"];}
	
	if (numeric == 31) {[self playSinglesound:@"31"];}
	if (numeric == 32) {[self playSinglesound:@"32"];}
	if (numeric == 33) {[self playSinglesound:@"33"];}
	if (numeric == 34) {[self playSinglesound:@"34"];}
	if (numeric == 35) {[self playSinglesound:@"35"];}
	if (numeric == 36) {[self playSinglesound:@"36"];}
	if (numeric == 37) {[self playSinglesound:@"37"];}
	if (numeric == 38) {[self playSinglesound:@"38"];}
	if (numeric == 39) {[self playSinglesound:@"39"];}
	if (numeric == 40) {[self playSinglesound:@"40"];}
	
	if (numeric == 41) {[self playSinglesound:@"41"];}
	if (numeric == 42) {[self playSinglesound:@"42"];}
	if (numeric == 43) {[self playSinglesound:@"43"];}
	if (numeric == 44) {[self playSinglesound:@"44"];}
	if (numeric == 45) {[self playSinglesound:@"45"];}
	if (numeric == 46) {[self playSinglesound:@"46"];}
	if (numeric == 47) {[self playSinglesound:@"47"];}
	if (numeric == 48) {[self playSinglesound:@"48"];}
	if (numeric == 49) {[self playSinglesound:@"49"];}
	if (numeric == 50) {[self playSinglesound:@"50"];}
	
	if (numeric == 51) {[self playSinglesound:@"51"];}
	if (numeric == 52) {[self playSinglesound:@"52"];}
	if (numeric == 53) {[self playSinglesound:@"53"];}
	if (numeric == 54) {[self playSinglesound:@"54"];}
	if (numeric == 55) {[self playSinglesound:@"55"];}
	if (numeric == 56) {[self playSinglesound:@"56"];}
	if (numeric == 57) {[self playSinglesound:@"57"];}
	if (numeric == 58) {[self playSinglesound:@"58"];}
	if (numeric == 59) {[self playSinglesound:@"59"];}
	
}

- (void)triggerAnnounce:(id)sender
{
	
	if (_iannouncenumber == 0) {
		[self playSinglesound:@"Time"];
	}
	
	if (_iannouncenumber == 1) {
		if (_12hourmode == true) {
			[self play12Hournumeric:_currenthour];
		} else {
			[self playNumericsound:_currenthour];
		}
		
	}
	
	if (_iannouncenumber == 2) {
		if (_currentminute == 0) {
			if (_12hourmode == true) {
				[self playSinglesound:@"oclock"];
			} else {
				[self playSinglesound:@"hundred"];
			}
		} else {
			[self playNumericsound:_currentminute];
		}
		
	}
	
	if (_12hourmode == true) {
		if (_iannouncenumber == 3) {
			if (_ispm == true) {
				[self playSinglesound:@"pm"];
			} else {
				[self playSinglesound:@"am"];
			}
		}
	}
	
	if (_iannouncenumber >= 4) {
		[announceTimer invalidate];
		_timeannouncementplaying = false;
	}
	
	_iannouncenumber++;
}

- (void)startAnnouncementTimer
{
	_iannouncenumber = 0;
	announceTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1.2 target:self selector:@selector(triggerAnnounce:) userInfo:nil repeats:TRUE];
	//clockTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
}

- (IBAction)btnAnnouncetime:(id)sender
{
	if (_timeannouncementplaying == false) {
		[self playSound:ClockButton];
		_timeannouncementplaying = true;
		[self startAnnouncementTimer];
	}
}

- (void)snoozeTick:(id)sender
{
	[self setAlertactive:true];
}

- (void)startSnooze
{
	// Set the snooze timer for 10 minutes
	_snoozetimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)600 target:self selector:@selector(snoozeTick:) userInfo:nil repeats:FALSE];
}

- (IBAction)btnHourmode:(id)sender
{
	[self playSinglesound:@"clockbutton"];
	if (_12hourmode == true) {
		_12hourmode = false;
	} else {
		_12hourmode = true;
	}
	
	[self refreshAmpm];
	[self refreshClockTick];
	//[self saveSettings];
}

- (IBAction)btnMinuteup:(id)sender
{
	[self alarmAdjust:MinuteUp];
}

- (IBAction)btnMinutedown:(id)sender
{
	[self alarmAdjust:MinuteDown];
}

- (IBAction)btnHourdown:(id)sender
{
	[self alarmAdjust:HourDown];
}

- (IBAction)btnHourup:(id)sender
{
	[self alarmAdjust:HourUp];
}

- (IBAction)alarmAdjustcancel:(id)sender
{
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void)alarmAdjust:(AlarmAdjustMode)adjustmode
{
	if (_alarmadjustedbytimer == false) {
		if (adjustmode == HourUp) {
			[self HourAdjust:false];
		}
		if (adjustmode == HourDown) {
			[self HourAdjust:true];
		}
		if (adjustmode == MinuteUp) {
			[self MinuteAdjust:false];
		}
		if (adjustmode == MinuteDown) {
			[self MinuteAdjust:true];
		}
	}
	
	if (_alarmadjusttimer != nil) {
		[_alarmadjusttimer invalidate];
		_alarmadjusttimer = nil;
	}
}

- (void) alarmAdjusttick:(id)sender
{
	if (_alarmadjustmode == HourUp)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:false];
	}
	
	if (_alarmadjustmode == HourDown)
	{
		_alarmadjustedbytimer = true;
		[self HourAdjust:true];
	}
	
	if (_alarmadjustmode == MinuteUp)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:false];
	}
	
	if (_alarmadjustmode == MinuteDown)
	{
		_alarmadjustedbytimer = true;
		[self MinuteAdjust:true];
	}
}

- (void)startAlarmadjust
{
	//[self HourAdjust:false];
	_alarmadjustedbytimer = false;
	_alarmadjusttimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)0.5 target:self selector:@selector(alarmAdjusttick:) userInfo:nil repeats:TRUE];
}

- (IBAction)btnMinuteuppressed:(id)sender
{
	[self playSound:ClockButtonHigh];
	_alarmadjustmode = MinuteUp;
	[self startAlarmadjust];
	//[self saveSettings];
}

- (IBAction)btnMinutedownpressed:(id)sender
{
	[self playSound:ClockButtonHigh];
	_alarmadjustmode = MinuteDown;
	[self startAlarmadjust];
	//[self saveSettings];
}

- (IBAction)btnHourdownpressed:(id)sender
{
	[self playSound:ClockButtonHigh];
	_alarmadjustmode = HourDown;
	[self startAlarmadjust];
	//[self saveSettings];
}

- (IBAction)btnHouruppressed:(id)sender
{
	[self playSound:ClockButtonHigh];
	_alarmadjustmode = HourUp;
	[self startAlarmadjust];
	//[self saveSettings];
}

- (IBAction)alarmSoundBackbuttontriggered:(id)sender
{
	_currentalarmsound--;
	
	if (_currentalarmsound < 1) {
		_currentalarmsound = 10;
	}
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}

- (IBAction)alarmSoundNextbuttontriggered:(id)sender
{
	_currentalarmsound++;
	
	if (_currentalarmsound > 10) {
		_currentalarmsound = 1;
	}
	
	
	[self loadAlarmsoundlabel:_currentalarmsound];
	[self setNeedsDisplay];
	[self playCurrentalarmsound];
}

- (void)playCurrentalarmsound
{
	if (_currentalarmsound == 1) {[self playSound:AlarmSound1];return;}
	if (_currentalarmsound == 2) {[self playSound:AlarmSound2];return;}
	if (_currentalarmsound == 3) {[self playSound:AlarmSound3];return;}
	if (_currentalarmsound == 4) {[self playSound:AlarmSound4];return;}
	if (_currentalarmsound == 5) {[self playSound:AlarmSound5];return;}
	if (_currentalarmsound == 6) {[self playSound:AlarmSound6];return;}
	if (_currentalarmsound == 7) {[self playSound:AlarmSound7];return;}
	if (_currentalarmsound == 8) {[self playSound:AlarmSound8];return;}
	if (_currentalarmsound == 9) {[self playSound:AlarmSound9];return;}
	if (_currentalarmsound == 10) {[self playSound:AlarmSound10];return;}
}

- (void)loadAlarmsoundlabel:(int)soundnumber
{
	if (soundnumber == 1) {alarmsoundlabel = [UIImage imageNamed:@"Snd1.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 2) {alarmsoundlabel = [UIImage imageNamed:@"Snd2.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 3) {alarmsoundlabel = [UIImage imageNamed:@"Snd3.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 4) {alarmsoundlabel = [UIImage imageNamed:@"Snd4.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 5) {alarmsoundlabel = [UIImage imageNamed:@"Snd5.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 6) {alarmsoundlabel = [UIImage imageNamed:@"Snd6.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 7) {alarmsoundlabel = [UIImage imageNamed:@"Snd7.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 8) {alarmsoundlabel = [UIImage imageNamed:@"Snd8.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 9) {alarmsoundlabel = [UIImage imageNamed:@"Snd9.png"]; [alarmsoundlabel retain];return;}
	if (soundnumber == 10) {alarmsoundlabel = [UIImage imageNamed:@"Snd10.png"]; [alarmsoundlabel retain];return;}
	
	alarmsoundlabel = nil;
}

- (IBAction)toggleSound:(id)sender
{
	[self playCurrentalarmsound];
	//[self saveSettings];
}

- (void)setAlarmactive:(bool)active
{
	if (active == true) 
	{
		img_alarmactive.hidden = false;
		img_alarmoff.hidden = true;
	} else {
		img_alarmactive.hidden = true;
		img_alarmoff.hidden = false;
	}
}

- (IBAction)toggleAlarmOnOff:(id)sender
{
	[self playSound:ClockButton];
	if (_alarmactive == true) 
	{
		_alarmactive = false;
	} else {
		_alarmactive = true;
	}
	
	[self setAlarmactive:_alarmactive];
	//[self saveSettings];
}

- (void)HourAdjust:(bool)down
{
	if (down == true) {
		_currentalarmhour--;
		
		if (_currentalarmhour <= -1) {
			_currentalarmhour = 23;
		}
		
	} else {
		++_currentalarmhour;
		
		if (_currentalarmhour >= 24) {
			_currentalarmhour = 0;
		}
	}
	
	//AudioServicesPlaySystemSound(_ssbuttonhigh);
	
	[self RefreshAlarmtime];
	[self setNeedsDisplay];
}

- (void)MinuteAdjust:(bool)down
{
	if (down == true) {
		_currentalarmminute--;
		
		if (_currentalarmminute <= -1) {
			_currentalarmminute = 59;
		}
		
	} else {
		++_currentalarmminute;
		
		if (_currentalarmminute >= 60) {
			_currentalarmminute = 0;
		}
	}
	
	//AudioServicesPlaySystemSound(_ssbuttonhigh);
	
	[self RefreshAlarmtime];
	[self setNeedsDisplay];
}

- (void)refreshAmpm
{
	if (_12hourmode == true) {
		if (_ispm == true) 
		{
			img_pm.hidden = false;
			img_am.hidden = true;
		} else {
			img_pm.hidden = true;
			img_am.hidden = false;
		}
	} else {
		img_am.hidden = true;
		img_pm.hidden = true;
	}
}

- (void)playSound:(ClockSound)soundtoplay
{
	if (soundtoplay == ClockButton)
	{
		[self playSinglesound:@"clockbutton"];
	}
	
	if (soundtoplay == ClockButtonHigh)
	{
		[self playSinglesound:@"clockbuttonhigh"];
	}
	
	if (soundtoplay == AlarmSound1)
	{
		[self playSinglesound:@"clocksound1"];
	}
	
	if (soundtoplay == AlarmSound2)
	{
		[self playSinglesound:@"clocksound3"];
	}
	
	if (soundtoplay == AlarmSound3)
	{
		[self playSinglesound:@"clocksound4"];
	}
	
	if (soundtoplay == AlarmSound4)
	{
		[self playSinglesound:@"clocksound5"];
	}
	
	if (soundtoplay == AlarmSound5)
	{
		[self playSinglesound:@"clocksound6"];
	}
	
	if (soundtoplay == AlarmSound6)
	{
		[self playSinglesound:@"clocksound7"];
	}
	
	if (soundtoplay == AlarmSound7)
	{
		[self playSinglesound:@"clocksound8"];
	}
	
	if (soundtoplay == AlarmSound8)
	{
		[self playSinglesound:@"clocksound10"];
	}
	
	if (soundtoplay == AlarmSound9)
	{
		[self playSinglesound:@"clocksound11"];
	}
	
	if (soundtoplay == AlarmSound10)
	{
		[self playSinglesound:@"clocksound12"];
	}
	
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
	_audioplaying = false;
	if (_myaudioplayer != nil)
	{
		NSLog (@"Releasing Audioplayer...");
		[_myaudioplayer release];
	}
}

- (void)playSinglesoundEx:(NSString *)resourcename
{
	if (_audioplaying == true) {
		if (_myaudioplayer != nil)
		{
			[_myaudioplayer stop];
			[_myaudioplayer release];
		}
	}
	
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
	
		_myaudioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL: url error: nil];		
		[url release];
		
		[_myaudioplayer setVolume:1];
		[_myaudioplayer setDelegate:self];
		[_myaudioplayer prepareToPlay];
		[_myaudioplayer play];	
		_audioplaying = true;
	}
	
	//NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:wavName ofType: @"wav"];
	
	//NSURL *fileURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
	
	
	//[fileURL release];
	
	//[_myaudioplayer setVolume:1];
	//[_myaudioplayer setDelegate:self];
	//[_myaudioplayer prepareToPlay];
	//[_myaudioplayer play];
	
	
}

- (void)playSinglesound:(NSString *)resourcename
{
	[self playSinglesoundEx:resourcename];
	
	
	/*
	AudioServicesDisposeSystemSoundID(_singlesound);
	NSString *buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"wav" inDirectory:@"/"];
	
	if (buttonPath == nil) {
		buttonPath = [[NSBundle mainBundle] pathForResource:resourcename ofType:@"WAV" inDirectory:@"/"];
	}
	
	if (buttonPath != nil) {
		CFURLRef sndbuttonURL;
		NSURL *url = [[NSURL alloc] initFileURLWithPath:buttonPath];
		sndbuttonURL = (CFURLRef)url;
		AudioServicesCreateSystemSoundID(sndbuttonURL, &_singlesound);
		[url release];
		
		AudioServicesPlaySystemSound(_singlesound);
	}
	*/
}

- (void)setAlarmvisible:(bool)visible
{
	if (visible == true)
	{
		// Hide the alarm settings
		img_alarmtime.hidden = true;
		btn_hourup.hidden = true;
		btn_hourdown.hidden = true;
		btn_minuteup.hidden = true;
		btn_minutedown.hidden = true;
		[_alarmnumbers setVisible:false];
		
		// Show the alert
		btn_dismiss.hidden = false;
		btn_snooze.hidden = false;
		img_alert.hidden = false;
		
		
		if (_landscapemode == true)
		{
			// Landscape
			// Hide the date
			[_daynumbers setVisible:false];
			[_yearnumbers setVisible:false];
			[_daystemplate setVisible:false];
			[_monthstemplate setVisible:false];
			
		} else {
			// Portrait
			// Hide the additional settings
			btn_snd.hidden = true;
			btn_alarmonoff.hidden = true;
			img_alarmoff.hidden = true;
			img_alarmactive.hidden = true;
		}
	} else {
		
		// Show the alarm settings
		img_alarmtime.hidden = false;
		btn_hourup.hidden = false;
		btn_hourdown.hidden = false;
		btn_minuteup.hidden = false;
		btn_minutedown.hidden = false;
		[_alarmnumbers setVisible:true];
		
		[_daynumbers setVisible:true];
		[_yearnumbers setVisible:true];
		[_daystemplate setVisible:true];
		[_monthstemplate setVisible:true];
		
		// Show the additional settings
		btn_snd.hidden = false;
		btn_alarmonoff.hidden = false;
		
		if (_alarmactive == true) {
			img_alarmoff.hidden = true;
			img_alarmactive.hidden = false;
		} else {
			img_alarmoff.hidden = false;
			img_alarmactive.hidden = true;
		}
		
		// Hide the alert
		btn_dismiss.hidden = true;
		btn_snooze.hidden = true;
		img_alert.hidden = true;

	}
	
	[self setNeedsDisplay];
}

- (void)setLandscapemode:(bool)landscape
{
	if (landscape == true) {
		NSLog(@"Orientation is landscape!");
		_landscapemode = true;
		//[self setupImagelocations:_landscapemode];
		landscapebtn_announcetime.hidden = false;
		portraitbtn_announcetime.hidden = true;
		
		
		//@synthesize btn_hourup; // 26 x 27, 392, 104
		//@synthesize btn_hourdown;  // 26 x 27, 364, 104
		//@synthesize btn_minuteup;  // 26 x 27, 446, 104
		//@synthesize btn_minutedown;  // 26 x 27, 418, 104
		//@synthesize btn_snd; // 40 x 32, 336, 180
		//@synthesize btn_alarmonoff; // 40 x 32, 336, 216
		//@synthesize img_alarmtime; // 70 x 52, 288, 51
		
		img_alarmtime.frame = CGRectMake(288, 51, 70, 52);
		btn_hourup.frame = CGRectMake(392, 104, 26, 27);
		btn_hourdown.frame = CGRectMake(364, 104, 26, 27);
		btn_minuteup.frame = CGRectMake(446, 104, 26, 27);
		btn_minutedown.frame = CGRectMake(418, 104, 26, 27);
		
		
		btn_snd.frame = CGRectMake(336, 180, 40, 32);
		btn_alarmonoff.frame = CGRectMake(336, 216, 40, 32);
		
		img_alarmoff.frame = CGRectMake (377, 219, 52, 26);
		img_alarmactive.frame = CGRectMake (377, 219, 52, 26);
		
		//img_snd1.frame = CGRectMake (377, 183, 52, 26);
		//img_snd2.frame = CGRectMake (377, 183, 52, 26);
		
		_alarmsoundlabel_location = CGPointMake(377, 183.0f);

		[_alarmnumbers setLocation:CGPointMake(368.0f, 51.0f)];
		[_secondnumbers setLocation:CGPointMake(310.0f, 256.0f)];
		[_daynumbers setLocation:CGPointMake(110.0f, 90.0f)];
		[_yearnumbers setLocation:CGPointMake(245.0f, 90.0f)];
		[_daystemplate setLocation:110.0f :55.0f];
		[_monthstemplate setLocation:135.0f :90.0f];
		
		btn_dismiss.frame = CGRectMake (120, 56, 80, 64);
		btn_snooze.frame = CGRectMake (202, 56, 80, 64);
		img_alert.frame = CGRectMake (287, 57, 139, 64);
		// Dismiss Button - 120, 56, 80, 64
		// Snooze Button - 202, 56, 80, 64
		// Alert Bitmap - 287, 57, 139, 64
		img_am.frame = CGRectMake (377, 256, 40, 28);
		img_pm.frame = CGRectMake (377, 256, 35, 30);
		
	} else {
		NSLog(@"Orientation is portrait!");
		_landscapemode = false;
		//[self setupImagelocations:_landscapemode];
		landscapebtn_announcetime.hidden = true;
		portraitbtn_announcetime.hidden = false;
		
		img_alarmtime.frame = CGRectMake(112, 330, 70, 52);
		btn_hourup.frame = CGRectMake(216, 383, 26, 27);
		btn_hourdown.frame = CGRectMake(188, 383, 26, 27);
		btn_minuteup.frame = CGRectMake(270, 383, 26, 27);
		btn_minutedown.frame = CGRectMake(242, 383, 26, 27);
		
		btn_snd.frame = CGRectMake(104, 428, 40, 32);
		btn_alarmonoff.frame = CGRectMake(214, 428, 40, 32);
		
		img_alarmoff.frame = CGRectMake (257, 431, 52, 26);
		img_alarmactive.frame = CGRectMake (257, 431, 52, 26);
		//img_snd1.frame = CGRectMake (145, 431, 52, 26);
		//img_snd2.frame = CGRectMake (145, 431, 52, 26);
		
		_alarmsoundlabel_location = CGPointMake(145, 431);
		
		//Dismiss Button - 153, 396, 80, 64
		//Snooze Button	 - 235, 396, 80, 64
		//Alert Bitmap - 143, 56, 139, 64
		btn_dismiss.frame = CGRectMake (131, 396, 80, 64);
		btn_snooze.frame = CGRectMake (213, 396, 80, 64);
		img_alert.frame = CGRectMake (146, 325, 139, 64);
		
		// am - 112, 294, 40, 28
		img_am.frame = CGRectMake (112, 294, 40, 28);
		img_pm.frame = CGRectMake (112, 294, 35, 30);
		
		[_alarmnumbers setLocation:CGPointMake(190.0f, 330.0f)];
		[_secondnumbers setLocation:CGPointMake(280.0f, 295.0f)];
		[_daynumbers setLocation:CGPointMake(110.0f, 90.0f)];
		[_yearnumbers setLocation:CGPointMake(245.0f, 90.0f)];
		[_daystemplate setLocation:110.0f :55.0f];
		[_monthstemplate setLocation:135.0f :90.0f];
	}
	
}

- (void) clockTick:(id)sender
{
	//NSLog (@"Clock is ticking...");
	//NSLog (@"Scale Factor: %@", self.contentScaleFactor);
	
	//[_clocknumbers setClockstring:@"21:47"];
	
	[self refreshClockTick];
	//[self setAlarmvisible:false];
	[self refreshAmpm];
	
	[self setNeedsDisplay];
	
}

- (void)GetNumericTime
{
	NSDate* date = [NSDate date];
	NSDateComponents *weekdayComponents =[_gregorian components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
	
	[_formatter setDateFormat:@"HH"];
	NSString* strhour = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"mm"];
	NSString* strminute = [_formatter stringFromDate:date];
	
	[_formatter setDateFormat:@"MM"];
	NSString* strmonth = [_formatter stringFromDate:date];
	
	_currentminute = [strminute intValue];
	_currenthour = [strhour intValue];
	_numericweekday = [weekdayComponents weekday];
	_numericmonth = [strmonth intValue];
	
	if (_currenthour == 0) {_ispm = false;}
	if (_currenthour == 1) {_ispm = false;}
	if (_currenthour == 2) {_ispm = false;}
	if (_currenthour == 3) {_ispm = false;}
	if (_currenthour == 4) {_ispm = false;}
	if (_currenthour == 5) {_ispm = false;}
	if (_currenthour == 6) {_ispm = false;}
	if (_currenthour == 7) {_ispm = false;}
	if (_currenthour == 8) {_ispm = false;}
	if (_currenthour == 9) {_ispm = false;}
	if (_currenthour == 10) {_ispm = false;}
	if (_currenthour == 11) {_ispm = false;}
	if (_currenthour == 12) {_ispm = true;}
	if (_currenthour == 13) {_ispm = true;}
	if (_currenthour == 14) {_ispm = true;}
	if (_currenthour == 15) {_ispm = true;}
	if (_currenthour == 16) {_ispm = true;}
	if (_currenthour == 17) {_ispm = true;}
	if (_currenthour == 18) {_ispm = true;}
	if (_currenthour == 19) {_ispm = true;}
	if (_currenthour == 20) {_ispm = true;}
	if (_currenthour == 21) {_ispm = true;}
	if (_currenthour == 22) {_ispm = true;}
	if (_currenthour == 23) {_ispm = true;}
}


- (void)RefreshAlarmtime
{
	NSNumber *mins;
	NSNumber *hours;
	
	[_strtime setString:@""];
	
	mins = [NSNumber numberWithInt:_currentalarmminute];
	hours = [NSNumber numberWithInt:_currentalarmhour];
	
	if ([hours intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[hours stringValue]];
	[_strtime appendString:@":"];
	
	if ([mins intValue] < 10) {
		[_strtime appendString:@"0"];
	}
	
	[_strtime appendString:[mins stringValue]];
	
	[_alarmnumbers setClockstring:_strtime];
	
	//[mins release];
	//[hours release];
	//[strtime release];
}


- (void)refreshClockTick
{
	//NSLog(@"Clock tick");
	
	[self GetNumericTime];
	
	NSDate* date = [NSDate date];
	
	if (_12hourmode == true) {
		[_formatter setDateFormat:@"HH:mm"];
		
		if (_currenthour == 13) {[_formatter setDateFormat:@"1:mm"];}
		if (_currenthour == 14) {[_formatter setDateFormat:@"2:mm"];}		
		if (_currenthour == 15) {[_formatter setDateFormat:@"3:mm"];}
		if (_currenthour == 16) {[_formatter setDateFormat:@"4:mm"];}
		if (_currenthour == 17) {[_formatter setDateFormat:@"5:mm"];}
		if (_currenthour == 18) {[_formatter setDateFormat:@"6:mm"];}
		if (_currenthour == 19) {[_formatter setDateFormat:@"7:mm"];}
		if (_currenthour == 20) {[_formatter setDateFormat:@"8:mm"];}
		if (_currenthour == 21) {[_formatter setDateFormat:@"9:mm"];}
		if (_currenthour == 22) {[_formatter setDateFormat:@"10:mm"];}
		if (_currenthour == 23) {[_formatter setDateFormat:@"11:mm"];}
		if (_currenthour == 0) {[_formatter setDateFormat:@"12:mm"];}
		
	} else {
		[_formatter setDateFormat:@"HH:mm"];
	}
	
	NSString* str = [_formatter stringFromDate:date];
	[_clocknumbers setClockstring:str];
	//[_clocknumbers setClockstring:@"13:26"];
	
	[_formatter setDateFormat:@"ss"];
	str = [_formatter stringFromDate:date];
	[_secondnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"d"];
	str = [_formatter stringFromDate:date];
	[_daynumbers setClockstring:str];
	
	[_formatter setDateFormat:@"yyyy"];
	str = [_formatter stringFromDate:date];
	[_yearnumbers setClockstring:str];
	
	[_formatter setDateFormat:@"EEEE"];
	str = [_formatter stringFromDate:date];
	[_daystemplate setNumericWeekday:_numericweekday];
	//[_daystemplate setNumericWeekday:6];
	
	
	[_formatter setDateFormat:@"MMMM"];
	str = [_formatter stringFromDate:date];
	[_monthstemplate setNumericMonth:_numericmonth];
	//[_monthstemplate setNumericMonth:1];
	
	if (_alertactive == false) {
	
		if (_alarmactive == true) {
			img_alarmoff.hidden = true;
			img_alarmactive.hidden = false;
		} else {
			img_alarmoff.hidden = false;
			img_alarmactive.hidden = true;
		}
	}
	
	[self refreshAmpm];
	
	if (_alarmactive == true) {
		
		if (_userdismissedalert == false) {
			if (_currentalarmhour == _currenthour)
			{
				if (_currentalarmminute == _currentminute) {
					
					if (_alertactive == false) {
						[self setAlertactive:_alarmactive];
					}
					
				}
			}
		}		
	}
	
	if (_currentalarmminute != _currentminute) {
		_userdismissedalert = false;
	}
	
	
}

- (void) startClocktick
{
	_clockticktimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)1 target:self selector:@selector(clockTick:) userInfo:nil repeats:TRUE];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
	
	if (alarmsoundlabel != nil) {
		if (alarmsoundlabel != nil) {

			//CGSize size = alarmsoundlabel.size; 
			
			//UIGraphicsBeginImageContext(size);
			
			//[alarmsoundlabel drawAtPoint:(CGPoint)_alarmsoundlabel_location:blendMode:kCGBlendModeNormal:alpha:0.5f];
			[alarmsoundlabel drawAtPoint:_alarmsoundlabel_location];
			
			//UIGraphicsEndImageContext();
		}
	}
	
	[_clocknumbers PaintString];
	[_alarmnumbers PaintString];
	[_secondnumbers PaintString];
	[_daynumbers PaintString];
	[_yearnumbers PaintString];
	[_daystemplate PaintString];
	[_monthstemplate PaintString];
}


- (void)dealloc {
    [super dealloc];
}


@end
