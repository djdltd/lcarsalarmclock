//
//  AlarmNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 20/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AlarmNumbers : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	bool _visible;
	UIImage *frame;
	CGPoint _location;
	CGFloat _charwidth;
	CGFloat _charheight;
	CGFloat _scalefactor;
	CGFloat _sepwidth;
}

- (void) setVisible:(bool)visible;
- (void) Update;
- (void) Initialise;
- (void) Paint;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;
- (void) setLocation:(CGPoint)location;


@end
