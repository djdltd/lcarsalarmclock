//
//  ClockNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 19/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ClockNumbers : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	UIImage *frame;
	UniChar singlechar;
	CGFloat _charwidth;
	CGFloat _charheight;
	CGFloat _scalefactor;
	CGFloat _sepwidth;
}

- (void) Update;
- (void) Initialise;
- (void) Paint;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;

@end
