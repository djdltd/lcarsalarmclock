//
//  AlarmNumbers.m
//  LCARS Clock
//
//  Created by Danny Draper on 20/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "AlarmNumbers.h"


@implementation AlarmNumbers

- (void) Initialise
{
	UIScreen *mainscr = [UIScreen mainScreen];
	NSLog (@"Scale of UIScreen: %f", mainscr.scale);
	
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	_visible = false;

	
	// 20 x 49
	_charwidth = 20.0f;
	_charheight = 49.0f;
	_scalefactor = mainscr.scale;
	_sepwidth = 20.0f;

	CGFloat startx = 7.0f*_scalefactor;
	CGFloat starty = 3.0f*_scalefactor;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	//CGPoint currentFramelocation;
	
	//CGFloat xlocation = 843.0f;
	//CGFloat ylocation = 541.0f;
	
	_clocknumbers = [NSMutableArray new];
	_curclockstring = [NSMutableString new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"AlarmNumbers"];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	
	// First level of numbers
	for (i=0;i<11;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(_sepwidth*_scalefactor, _charheight*_scalefactor);
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect) scale:_scalefactor orientation:UIImageOrientationUp];
		
		[_clocknumbers addObject:frame1];
		
		xoffset+=_charwidth*_scalefactor;
	}
	
}

- (void) setClockstring:(NSString *)clockstring
{
	[_curclockstring setString:clockstring];
	
}

- (void) setVisible:(bool)visible
{
	_visible = visible;
}

- (void) setLocation:(CGPoint)location
{
	_location = location;
}

- (void) PaintString
{
	if (_visible == false)
	{
		return;
	}
	
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xincrement = _sepwidth;
	
	CGFloat xlocation = _location.x;
	CGFloat ylocation = _location.y;
	
	for (c=0;c<[_curclockstring length];++c)
	{
		UniChar singlechar = [_curclockstring characterAtIndex:c];
		
		if (singlechar == ':') {
			frame = [_clocknumbers objectAtIndex:0];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '0') {
			frame = [_clocknumbers objectAtIndex:1];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '1') {
			frame = [_clocknumbers objectAtIndex:2];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '2') {
			frame = [_clocknumbers objectAtIndex:3];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '3') {
			frame = [_clocknumbers objectAtIndex:4];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '4') {
			frame = [_clocknumbers objectAtIndex:5];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '5') {
			frame = [_clocknumbers objectAtIndex:6];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '6') {
			frame = [_clocknumbers objectAtIndex:7];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '7') {
			frame = [_clocknumbers objectAtIndex:8];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '8') {
			frame = [_clocknumbers objectAtIndex:9];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '9') {
			frame = [_clocknumbers objectAtIndex:10];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
	}
}

- (void) Update
{
	NSNumber *mins;
	NSNumber *hours;
	
	_dmin++;	
	_dhour++;
	
	if (_dmin > 98) {
		_dmin = 0;
		//_dhour++;
	}
	
	if (_dhour > 98) {
		_dhour = 0;
		//_dmin = 0;
	}
	
	mins = [NSNumber numberWithInt:_dmin];
	hours = [NSNumber numberWithInt:_dhour];
	
	NSMutableString *strtime;
	
	strtime = [NSMutableString new];
	
	[strtime appendString:[mins stringValue]];
	[strtime appendString:@":"];
	[strtime appendString:[hours stringValue]];
	
	_curclockstring = strtime;
	
	//[mins release];
	//[hours release];
	//[strtime release];
	
}



- (void) Paint
{
	
}


@end
