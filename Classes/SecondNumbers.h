//
//  SecondNumbers.h
//  LCARS Clock
//
//  Created by Danny Draper on 21/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SecondNumbers : NSObject {

	NSMutableArray *_clocknumbers;
	int _curframe;
	NSMutableString *_curclockstring;
	int _dmin;
	int _dhour;
	bool _visible;
	UIImage *frame;
	CGPoint _location;
	CGFloat _charwidth;
	CGFloat _charheight;
	CGFloat _scalefactor;
	CGFloat _sepwidth;
	NSMutableString *_imageresource;
}

- (void) setVisible:(bool)visible;
- (void) Update;
- (void) Initialise:(CGFloat)offx: (CGFloat)offy: (CGFloat) charwidth: (CGFloat) charheight: (CGFloat) sepwidth: (NSString *) resourcename;
- (void) Paint;
- (void) setClockstring:(NSString *)clockstring;
- (void) PaintString;
- (void) setLocation:(CGPoint)location;


@end
