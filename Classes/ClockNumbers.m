//
//  ClockNumbers.m
//  LCARS Clock
//
//  Created by Danny Draper on 19/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "ClockNumbers.h"


@implementation ClockNumbers

- (void) Initialise
{
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	UIScreen *mainscr = [UIScreen mainScreen];
	NSLog (@"Scale of UIScreen: %f", mainscr.scale);
	
	CGFloat startx = 1.0f;
	CGFloat starty = 2.0f;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	_sepwidth = 39.0f;
	_charwidth = 48.0f;
	_charheight = 97.0f;
	_scalefactor = mainscr.scale;
	
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	_curclockstring = [NSMutableString new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"ClockNumbers"];
	
	NSLog (@"Scale of clocknumbers: %@", [clocknumbers scale]);
	

	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	
	// First level of numbers
	for (i=0;i<7;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(_sepwidth*_scalefactor, _charheight*_scalefactor);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect) scale:_scalefactor orientation:UIImageOrientationUp];
		
		[_clocknumbers addObject:frame1];
		
		xoffset+=_charwidth*_scalefactor;
	}
	
	xoffset = 0;
	yoffset+=_charheight*_scalefactor;
	
	// Second level of numbers
	for (i=0;i<5;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(_sepwidth*_scalefactor, _charheight*_scalefactor);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect) scale:_scalefactor orientation:UIImageOrientationUp];
		
		
		[_clocknumbers addObject:frame1];
		
		xoffset+=_charwidth*_scalefactor;
	}
}

- (void) setClockstring:(NSString *)clockstring
{
	[_curclockstring setString:clockstring];
	
}

- (void) PaintString
{
	int c = 0;
	
	CGFloat xoffset = 0.0f;
	CGFloat xincrement = _sepwidth;
	
	CGFloat xlocation = 110.0f;
	CGFloat ylocation = 190.0f;
	
	for (c=0;c<[_curclockstring length];++c)
	{
		singlechar = [_curclockstring characterAtIndex:c];
		
		if (singlechar == ':') {
			frame = [_clocknumbers objectAtIndex:0];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;
		}
		
		if (singlechar == '0') {
			frame = [_clocknumbers objectAtIndex:1];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '1') {
			frame = [_clocknumbers objectAtIndex:2];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '2') {
			frame = [_clocknumbers objectAtIndex:3];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '3') {
			frame = [_clocknumbers objectAtIndex:4];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '4') {
			frame = [_clocknumbers objectAtIndex:5];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '5') {
			frame = [_clocknumbers objectAtIndex:6];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '6') {
			frame = [_clocknumbers objectAtIndex:7];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '7') {
			frame = [_clocknumbers objectAtIndex:8];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '8') {
			frame = [_clocknumbers objectAtIndex:9];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
		
		if (singlechar == '9') {
			frame = [_clocknumbers objectAtIndex:10];
			[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
			xoffset+=xincrement;			
		}
	}
}

- (void) Update
{
	NSNumber *mins;
	NSNumber *hours;
	
	_dmin++;	
	_dhour++;
	
	if (_dmin > 98) {
		_dmin = 0;
		//_dhour++;
	}
	
	if (_dhour > 98) {
		_dhour = 0;
		//_dmin = 0;
	}
	
	mins = [NSNumber numberWithInt:_dmin];
	hours = [NSNumber numberWithInt:_dhour];
	
	NSMutableString *strtime;
	
	strtime = [NSMutableString new];
	
	[strtime appendString:[mins stringValue]];
	[strtime appendString:@":"];
	[strtime appendString:[hours stringValue]];
	
	_curclockstring = strtime;
	
	//[mins release];
	//[hours release];
	//[strtime release];
	
}



- (void) Paint
{
	
}

@end
