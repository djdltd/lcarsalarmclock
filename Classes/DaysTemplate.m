//
//  DaysTemplate.m
//  LCARS Clock
//
//  Created by Danny Draper on 22/07/2010.
//  Copyright 2010 CedeSoft Ltd. All rights reserved.
//

#import "DaysTemplate.h"


@implementation DaysTemplate

- (void) Initialise
{
	UIScreen *mainscr = [UIScreen mainScreen];
	NSLog (@"Scale of UIScreen: %f", mainscr.scale);
	
	_curframe = 0;
	_dmin = 0;
	_dhour = 0;
	
	_scalefactor = mainscr.scale;
	
	CGFloat spritewidth = 86.0*_scalefactor;
	CGFloat spriteheight = 23.0f*_scalefactor;
	
	CGFloat startx = 3.0f*_scalefactor;
	CGFloat starty = 3.0f*_scalefactor;
	
	CGFloat yoffset = 0.0f;
	CGFloat xoffset = 0.0f;
	
	
	//CGPoint currentFramelocation;
	
	_clocknumbers = [NSMutableArray new];
	_curclockstring = [NSMutableString new];
	
	UIImage *clocknumbers;
	
	clocknumbers = [UIImage imageNamed:@"Days"];
	
	
	CGRect imageRect;
	UIImage *frame1;
	
	int i = 0;
	_xloc = 10.0f;
	_yloc = 10.0f;
	
	// First level of numbers
	for (i=0;i<7;++i)
	{
		imageRect.origin = CGPointMake (startx + xoffset, starty + yoffset);
		imageRect.size = CGSizeMake(spritewidth, spriteheight);	
		frame1 = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([clocknumbers CGImage], imageRect) scale:_scalefactor orientation:UIImageOrientationUp];
		
		[_clocknumbers addObject:frame1];
		
		//xoffset+=25.0f;
		yoffset += spriteheight;
	}
}

- (void) setVisible:(bool)visible
{
	_visible = visible;
}

- (void) setLocation:(CGFloat) xloc:(CGFloat) yloc
{
	_xloc = xloc;
	_yloc = yloc;
}

- (void) setNumericWeekday:(int)weekday
{
	_iweekday = weekday;
}

- (void) setClockstring:(NSString *)clockstring
{
	[_curclockstring setString:clockstring];
	
}

- (void) PaintString
{
	if (_visible == false) {
		return;
	}
	
	CGFloat xoffset = 0.0f;
	CGFloat xlocation = _xloc;
	CGFloat ylocation = _yloc;
	
	
	/*
	 if ([_curclockstring rangeOfString:@"Saturday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:0];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Sunday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:1];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Monday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:2];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Tuesday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:3];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Wednesday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:4];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Thursday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:5];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 
	 if ([_curclockstring rangeOfString:@"Friday"].location != NSNotFound) {
	 frame = [_clocknumbers objectAtIndex:6];
	 [frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	 }
	 */
	
	if (_iweekday == 1) { // Sunday
		frame = [_clocknumbers objectAtIndex:1];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 2) { // Monday
		frame = [_clocknumbers objectAtIndex:2];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 3) { // Tuesday
		frame = [_clocknumbers objectAtIndex:3];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 4) { // Wednesday
		frame = [_clocknumbers objectAtIndex:4];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 5) { // Thursday
		frame = [_clocknumbers objectAtIndex:5];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 6) { // Friday
		frame = [_clocknumbers objectAtIndex:6];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
	
	if (_iweekday == 7) { // Saturday
		frame = [_clocknumbers objectAtIndex:0];
		[frame drawAtPoint:CGPointMake(xlocation + xoffset, ylocation)];
	}
}

- (void) Update
{
	NSNumber *mins;
	NSNumber *hours;
	
	_dmin++;	
	_dhour++;
	
	if (_dmin > 98) {
		_dmin = 0;
		//_dhour++;
	}
	
	if (_dhour > 98) {
		_dhour = 0;
		//_dmin = 0;
	}
	
	mins = [NSNumber numberWithInt:_dmin];
	hours = [NSNumber numberWithInt:_dhour];
	
	NSMutableString *strtime;
	
	strtime = [NSMutableString new];
	
	[strtime appendString:[mins stringValue]];
	[strtime appendString:@":"];
	[strtime appendString:[hours stringValue]];
	
	_curclockstring = strtime;
	
	//[mins release];
	//[hours release];
	//[strtime release];
	
}



- (void) Paint
{
	
}

@end
